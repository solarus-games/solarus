/**
\page lua_api_audio Audio

\tableofcontents

You can play musics and sound effects through \c sol.audio.

\section lua_api_audio_functions Functions of sol.audio

\subsection lua_api_audio_play_sound sol.audio.play_sound(sound_id)

Plays a sound effect.

Generates a Lua error if the sound does not exist.

Multiple sounds can be played in parallel.
But to get more control of your sound effect being played,
especially if you want to pause or stop it,
you can use the more powerful \ref lua_api_sound "sol.sound module"
instead of this function.

Unlike music files, sound files are entirely read before being played.
- \c sound_id (string): Name of the sound file to play, relative to the
  \c sounds directory and without extension.
  Currently, only OGG Vorbis (<tt>.ogg</tt>) sound files are supported.

\remark This is equivalent to <tt>sol.sound.create(sound_id):play()</tt>.

\subsection lua_api_audio_preload_sounds sol.audio.preload_sounds()

Loads all sounds effects into memory for faster future access.

\warning This method is deprecated since Solarus 2.0 and has no effect anymore.
  Sound effects are always automatically preloaded now.

\subsection lua_api_audio_play_music sol.audio.play_music(music_id, [action])

Plays a music.

If the music does not exist, a Lua error is generated.

Only one music can be played at a time.
If the same music was already playing, it continues normally and does not
restart.
If a different music was already playing, it is stopped and replaced by
the new one.

When the music reaches the end, the \c action parameter indicates what to do
next. The default behavior is to loop from the beginning.

However, some music files already have their own loop internal loop information.
Such musics are able to loop to a specific point rather than
to the beginning.
Since they already loop forever internally, they don't have an end
and the \c action parameter has no effect on them.
See \ref quest_musics_loop "Music loop settings"
to know how Solarus supports internal loop information for each format.

- \c music_id (string): Name of the music file to play, relative to the
  \c musics directory and without extension. The following extensions will be
  tried in this order: \c <tt>.ogg</tt>, \c <tt>.it</tt> and \c <tt>.spc</tt>.
  \c nil stops playing any music (the second parameter has no effect in this case).
  If you set the music name to the same music that is already playing, or to
  the special value \c "same", then this function does nothing:
  the music keeps playing (it does not restart) and the second parameter
  is ignored.
- \c action (function or boolean, optional): What to do when the music
  finishes (reaches its end).
  A boolean value indicates whether or not the music should loop.
  The default is \c true.
  A function value indicates a custom action (and implies no loop).
  It will be called when the music finishes.
  This allows you to perform an action of your choice, like playing
  another music.
  Note however that there might be some small delay between the exact
  end of the music and the time when your function is called,
  because the music can end between simulation frames
  but your function will be called the frame after that.

\subsection lua_api_audio_get_music sol.audio.get_music()

Returns the name of the music currently playing.

- Return value (string): Name of the music file currently playing, relative to
  the \c musics directory and without extension. Returns \c nil if no music is
  playing.

\subsection lua_api_audio_stop_music sol.audio.stop_music()

Stops playing music.

This function has no effect if no music was playing.

\remark This is equivalent to <tt>sol.audio.play_music("none")</tt>.

\subsection lua_api_audio_get_sound_volume sol.audio.get_sound_volume()

Returns the global volume of sound effects.
- Return value (number): The global volume of sound effects,
  as an integer between \c 0 (mute) and \c 100 (full volume).

\subsection lua_api_audio_set_sound_volume sol.audio.set_sound_volume(volume)

Sets the default volume of sound effects.

Note that individual sound effects have their own volume relative to this
global sound volume.
- \c volume (number): The new default volume of sound effects, as an integer
  between \c 0 (mute) and \c 100 (full volume).

\subsection lua_api_audio_get_music_volume sol.audio.get_music_volume()

Returns the current volume of musics.

This volume applies to all musics played by \ref lua_api_audio_play_music.
"sol.audio.play_music()"
- Return value (number): The current volume of musics, as an integer
  between \c 0 (no sound effects) and \c 100 (full volume).

\subsection lua_api_audio_set_music_volume sol.audio.set_music_volume(volume)

Sets the volume of musics.

This volume applies to all musics played by \ref lua_api_audio_play_music.
"sol.audio.play_music()"
- \c volume (number): The new volume of musics, as an integer
  between \c 0 (no music) and \c 100 (full volume).

\subsection lua_api_audio_get_music_format sol.audio.get_music_format()

Returns the format of the music currently playing.
- Return value (string): Format of the music: \c "ogg", \c "it" or \c "spc".
  Returns \c nil if no music is playing.

\subsection lua_api_audio_get_music_num_channels sol.audio.get_music_num_channels()

Returns the number of channels of the current .it music.

This function is only supported for .it musics.
- Return value (number): Number of channels of the music.
  Returns \c nil if the current music format is not .it.

\subsection lua_api_audio_get_music_channel_volume sol.audio.get_music_channel_volume(channel)

Returns the volume of notes of a channel for the current .it music.

This function is only supported for .it musics.
- \c channel (number): Index of a channel (the first one is zero).
- Return value (number): Volume of the channel.
  Returns \c nil if the current music format is not .it.

\remark The channel should have the same volume for all its notes.
  Otherwise, calling this function does not make much sense.

\subsection lua_api_audio_set_music_channel_volume sol.audio.set_music_channel_volume(channel, volume)

Sets the volume of all notes of a channel for the current .it music.

This function has no effect for musics other than .it.
- \c channel (number): Index of a channel (the first one is zero).
- \c volume (number): The volume to set.

\subsection lua_api_audio_get_tempo sol.audio.get_tempo()

Returns the tempo of the current .it music.

This function is only supported for .it musics.
- Return value (number): Tempo of the music.
  Returns \c nil if the current music format is not .it.

\subsection lua_api_audio_set_tempo sol.audio.set_tempo(tempo)

Sets the tempo of the current .it music.

This function is only supported for .it musics.
- \c tempo (number): Tempo to set.

*/

