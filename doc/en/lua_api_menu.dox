/**
\page lua_api_menu Menus

\tableofcontents

To display various information such as a title screen,
a dialog box,
a HUD (head-up display) or a pause screen, you can use one or several menus.

A menu is an arbitrary Lua table.
A menu belongs to a context that may be the current
\ref lua_api_map "map",
the current \ref lua_api_game "game",
the \ref lua_api_main "sol.main" table
or even another menu.
This context is the lifetime of your menu.
As long as your menu is active,
the engine will call events that are defined in your table,
i.e. callback methods like
\ref lua_api_menu_on_started "menu:on_started()",
\ref lua_api_menu_on_draw "menu:on_draw()",
\ref lua_api_input_on_key_pressed "menu:on_key_pressed()",
etc.,
to notify your menu of something
(the player pressed a key, your menu needs to be redrawn, etc.).

This menu API does not provide anything fundamental:
indeed, the
\ref lua_api_map "map",
\ref lua_api_game "game" and
\ref lua_api_main "sol.main"
APIs already provide the necessary features,
so you could do what you want from there manually.
But the API described on this page makes your life easier
because menus automatically receive events whenever they need to be notified,
and automatically stop being active when their context no longer exists.

\section lua_api_menu_functions Functions of sol.menu

\subsection lua_api_menu_start sol.menu.start(context, menu, [on_top])

Starts a menu in a context.

The Solarus engine will then call the appropriate events on your menu until
it is stopped.
- \c context (\ref lua_api_map "map", \ref lua_api_game "game" or table):
  The context your menu will belong to.
  Similarly to the case of \ref lua_api_timer "timers", the context determines
  the lifetime of your menu.
  The context must be one of the following four objects:
  - If you make a \ref lua_api_map "map" menu, your menu will be
    drawn above the map surface. It will be stopped when the player goes to
    another map.
    This may be useful to show head-up information local to a precise map.<br>
    Example: a counter or a mini-game that only exists on a specific map.<br>
  - If you make a \ref lua_api_game "game" menu, your menu will be
    global to all maps.
    As long as the game is running, it will persist accross map changes.<br>
    Example: the player's life counter.<br>
  - If you make a \ref lua_api_main "main" menu, your menu will be
    global to the whole program. It can exist outside a game (and it
    even persists during the game if you don't stop it).<br>
    Example: the title screen.
  - If you set the context to another menu, then its lifetime will be limited
    to this other menu.
    This allows to make nested menus.
    Example: a popup that shows some information above another menu.
- \c menu (table): The menu to activate. It can be any table. The only thing
  that makes it special is the presence of callback functions (events) as
  described in section \ref lua_api_menu_events.
- \c on_top (boolean, optional): Whether this menu should be drawn on top
  of other existing menus of the same context or behind them.
  If \c true, the
  \ref lua_api_menu_on_draw "on_draw()" event of your menu will be the last
  to be called if there are several menus in the context.
  If \c false, it will be the first one.
  No value means \c true.

\subsection lua_api_menu_stop sol.menu.stop(menu)

Stops a menu previously activated with \ref lua_api_menu_start
"sol.menu.start()".

After this call, the Solarus engine will no longer call events on your menu.
But you can restart it later if you want.

Nothing happens is the menu was already stopped or never started.
- \c menu (table): The menu to stop.

\subsection lua_api_menu_stop_all sol.menu.stop_all(context)

Stops all menus that are currently running in a context.
- \c context (\ref lua_api_map "map", \ref lua_api_game "game",
  \ref lua_api_main "sol.main" or table): The context where you want to stop
  menus.

This function is not often needed since menus are already automatically
stopped when their context is closed.

\subsection lua_api_menu_is_started sol.menu.is_started(menu)

Returns whether a table is a currently active menu.
- \c menu (table): The menu to test.
- Return value (boolean): \c true if this is an active menu.

\subsection lua_api_menu_bring_to_front sol.menu.bring_to_front(menu)

Places this menu in front of other menus of the same context.

Your menu will then be the first one to receive input events
(keyboard, joypad and mouse events)
and the last one to be drawn.
- \c menu (table): The menu to bring to the front.

\subsection lua_api_menu_bring_to_back sol.menu.bring_to_back(menu)

Places this menu behind other menus of the same context.

Your menu will then be the last one to receive input events
(keyboard, joypad and mouse events)
and the first one to be drawn.
- \c menu (table): The menu to bring to the back.

\section lua_api_menu_events Events of a menu

Events are callback methods automatically called by the engine if you define
them.

\subsection lua_api_menu_on_started menu:on_started()

Called when your menu is started.

This event is triggered when you call \ref lua_api_menu_start
"sol.menu.start()".

\subsection lua_api_menu_on_finished menu:on_finished()

Called when your menu is stopped.

This event is triggered when you call \ref lua_api_menu_stop "sol.menu.stop()"
or when the context of your menu finishes.

\subsection lua_api_menu_on_update menu:on_update()

Called at each cycle of the main loop while your menu is active.

Menus of are updated in the following order:
-# \ref lua_api_map "Map" menus (only during a game).
-# \ref lua_api_game "Game" menus (only during a game).
-# \ref lua_api_main "Main" menus (the more general ones).

When several menus exist in the same context, they are updated from the back
one to the front one.
You can control this order thanks to the
\c on_top parameter of
\ref lua_api_menu_start "menu:start()" when you start a menu.

\remark As this function is called at each cycle, it is recommended to use other
solutions when possible, like \ref lua_api_timer "timers" and other events.

\subsection lua_api_menu_on_draw menu:on_draw(dst_surface)

Called when your menu has to be redrawn.

Use this event to draw your menu.
- \c dst_surface (\ref lua_api_surface "surface"): The surface where you
  should draw your menu.

Menus of are drawn in the following order:
-# \ref lua_api_map "Map" menus (only during a game).
-# \ref lua_api_game "Game" menus (only during a game).
-# \ref lua_api_main "Main" menus (the more general ones).

When several menus exist in the same context, they are drawn from the back
one to the front one.
You can control this order thanks to the
\c on_top parameter of
\ref lua_api_menu_start "sol.menu.start()" when you start a menu,
or with \ref lua_api_menu_bring_to_front "sol.menu.bring_to_front()"
and \ref lua_api_menu_bring_to_back "sol.menu.bring_to_back()"

\subsection lua_api_menu_on_command_pressed menu:on_command_pressed(command)

Called during a game when the player presses a
\ref lua_api_game_overview_commands "game command"
(a keyboard key or a joypad action mapped to a built-in game behavior).
You can use this event to override the normal built-in behavior of the game
command.
Menus on top are notified first.
- \c command (string): Name of the built-in game command that was pressed (see
  the \ref lua_api_game_overview_commands "game API" for the list of existing
  game commands).
- Return value (boolean): Indicates whether the event was handled. If you
  return \c true, the event won't be propagated to other objects
  (you are overriding the built-in behavior of pressing this game command).

\remark As the notion of game commands only exist during a game,
  this event is only called for game menus and map menus.

\remark This event is not triggered if you already handled the underlying
  low-level keyboard or joypad event.

\subsection lua_api_menu_on_command_released menu:on_command_released(command)

Called during a game when the player released a game command
(a keyboard key or a joypad action mapped to a built-in game behavior).
You can use this event to override the normal built-in behavior of the game
command.
Menus on top are notified first.
- \c command (string): Name of the built-in game command that was released (see
  the \ref lua_api_game_overview_commands "game API" for the list of existing
  game commands).
- Return value (boolean): Indicates whether the event was handled. If you
  return \c true, the event won't be propagated to other objects
  (you are overriding the built-in behavior of releasing this game command).

\remark As the notion of game commands only exist during a game,
  this event is only called for game menus and map menus.

\remark This event is not triggered if you already handled the underlying
  low-level keyboard or joypad event.

\section lua_api_menu_input_handler_events Events as an Input Handler

In addition to its own events a menu is also an input handler and receives
all of those events.

Menus are checked to see if they handle an input event after their context is
checked. Unless their context is another menu, then they are checked first.

See \ref lua_api_input_handler_events to see all of these events.

*/
