/**
\page lua_api_music Music

\tableofcontents

You can create, play, or pause musics through \c sol.music.

An object of type \c music represents a music with its current playing state.
Multiple musics effects can be played, paused, resumed or
stopped independently.

\section lua_api_music_functions Functions of sol.music

\subsection lua_api_music_create sol.music.create(music_id)

Creates a new music but does not play it.

Generates a Lua error if the music file does not exist.

- \c music_id (string): Name of the music file to read, relative to
  the \c musics directory and without extension.
- Return value (music): The music object.

\section lua_api_music_methods Methods of sol.music

\subsection lua_api_music_play music:play()

Starts playing the music.

\subsection lua_api_music_stop music:stop()

Stops playing the music.

\subsection lua_api_music_get_volume music:get_volume()

Returns the volume for this music.
- Return value (integer): The volume between 0 and 100.

\subsection lua_api_music_set_volume music:set_volume(volume)

Sets the volume for this music relative to the global volume (see sol.audio.set_music_volume).
- \c volume (integer): The volume between 0 and 100.

\subsection lua_api_music_get_channel_volume music:get_channel_volume(channel)

Returns the volume for a specified channel.
- Return value (integer): The volume between 0 and 64 (like in your tracker software).

\subsection lua_api_music_set_channel_volume music:set_channel_volume(channel, volume)

Sets the volume for a specified channel.
- \c volume (integer): The volume between 0 and 64 (like in your tracker software).

\subsection lua_api_music_get_channel_pan music:get_channel_pan(channel)

Returns the pan for a specified channel.
- Return value (integer): The pan between 0 (left) and 256 (right).

\subsection lua_api_music_set_channel_pan music:set_channel_pan(channel, pan)

Sets the pan for a specified channel.
- \c volume (integer): The pan between 0 (left) and 256 (right).

*/