---@class tile : entity
---
---Tiles are the small fixed bricks that compose the [map](https://doxygen.solarus-games.org/latest/lua_api_map.html).
---
local m = {}

_G.tile = m

return m